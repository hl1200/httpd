<!DOCTYPE html>
<html>
<head>
<title><#Web_Title#> - <#menu4_2#> : <#menu4_2_1#></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">

<link rel="shortcut icon" href="images/favicon.ico">
<link rel="icon" href="images/favicon.png">
<link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/bootstrap/css/main.css">

<script type="text/javascript" src="/jquery.js"></script>
<script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/bootstrap/js/highcharts.js"></script>
<script type="text/javascript" src="/bootstrap/js/highcharts_theme.js"></script>
<script type="text/javascript" src="/state.js"></script>
<script>
    function initial(){
        alert('ok');
        show_banner(0);
}

</script>


</head>

<body>
    <h1>hello world</h1>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3">
                    <!--Sidebar content-->
                    <!--=====Beginning of Main Menu=====-->
                    <div class="well sidebar-nav side_nav" style="padding: 0px;">
                        <p>this is span3</p>
                    </div>
                </div>
    
                <div class="span9">
                    <!--Body content-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="box well grad_colour_dark_blue">
                                <h2 class="box_head round_top"><#menu4#> - <#menu4_2_1#></h2>
                                <div class="round_bottom">
                                    <div class="row-fluid">
                                        <div class="submenuBlock">
                                            <p>tabMenu</p>
                                        </div>
    
                                        <table width="100%" align="center" cellpadding="4" cellspacing="0" class="table table-stat">
                                            <tr>
                                                <th width="9%"><#Color#></th>
                                                <th width="11%"><#Network#></th>
                                                <th width="20%" style="text-align: right"><#Current#></th>
                                                <th width="20%" style="text-align: right"><#Average#></th>
                                                <th width="20%" style="text-align: right"><#Maximum#></th>
                                                <th width="20%" style="text-align: right"><#Total#></th>
                                            </tr>
                                        </table>
    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>

</body>
</html>
