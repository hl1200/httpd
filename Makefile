#ifndef ROOTDIR
#ROOTDIR=../..
#endif

#USERDIR = $(ROOTDIR)/user
#SHDIR = $(ROOTDIR)/user/shared

#include $(SHDIR)/boards.mk
#include $(SHDIR)/cflags.mk

#头文件目录，这里不同了
#CFLAGS += -Wall -I. -I$(SHDIR) -I$(SHDIR)/include
CFLAGS += -g
#lib和lib目录
#LDFLAGS += -L. -lm
#LDFLAGS += -L$(SHDIR) -lshared
#LDFLAGS += -L$(USERDIR)/wireless_tools -liw
#ifeq ($(STORAGE_ENABLED),y)
#CFLAGS  += -I$(USERDIR)/libdisk
#LDFLAGS += -L$(USERDIR)/libdisk -ldisk
#endif
#ifeq ($(CONFIG_FIRMWARE_INCLUDE_HTTPS),y)
#LDFLAGS += -L$(STAGEDIR)/lib -lssl -lcrypto
#endif

#可执行文件
EXEC = httpd
#.o文件
#OBJS = httpd.o ej.o cgi.o web_ex.o common.o nvram_x.o ralink.o crc32.o base64.o aspbw.o initial_web_hook.o variables.o
#OBJS += tdate_parse.o upload.o
OBJS = httpd.o base64.o
OBJS += tdate_parse.o 
#ifeq ($(CONFIG_FIRMWARE_INCLUDE_HTTPS),y)
#OBJS += https.o
#endif
#网盘 暂时不要
#ifeq ($(STORAGE_ENABLED),y)
#OBJS += aidisk.o
#endif
#rtl8367 vlan交换机，暂时不要
#ifdef CONFIG_RTL8367
#OBJS += switch_rtl8367.o
#CFLAGS += -I$(ROOTDIR)/$(LINUXDIR)/drivers/net/rtl8367
#else
#ifdef CONFIG_RAETH
#OBJS += switch_mtk_esw.o
#CFLAGS += -I$(ROOTDIR)/$(LINUXDIR)/drivers/net/ethernet/raeth
#else
#ifdef CONFIG_RAETH_ESW
#OBJS += switch_mtk_esw.o
#CFLAGS += -I$(ROOTDIR)/$(LINUXDIR)/drivers/net/raeth
#endif
#endif
#endif
#OBJS += dbapi.o
#添加CC变量
all: $(OBJS) 
	$(CC) -o $(EXEC) $(OBJS) $(LDFLAGS)

c.o:
	$(CC) -c $*.c $(CFLAGS)

clean:
	rm -f *.o *~ httpd

#romfs:
#	$(ROMFSINST) /usr/sbin/httpd
#ifeq ($(CONFIG_FIRMWARE_INCLUDE_HTTPS),y)
#	$(ROMFSINST) /usr/bin/https-cert.sh
#endif
